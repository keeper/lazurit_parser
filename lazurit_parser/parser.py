from .solver import Solver
import sys
import json
import os
from .exceptions import FileNotFoundException, WrongValueFormat, ValueOutOfRangeException, ParserException, \
    ParsingException
import re
from .lazurit import Checkdes, AC, VC, VCS, BC, BCS, Drossel, XCut, Prepost


class Parser:
    def __init__(self):
        pass

    def from_json(self, path_to_json):
        solver = Solver()
        if not os.path.exists(path_to_json):
            raise FileNotFoundException(f"Parser unable to find json file {path_to_json}")
        with open(path_to_json, 'r') as file:
            js = json.load(file)
        self._load_check(js["checkdes"].values())
        self._load_AC(js["ac"].values())
        self._load_VC(js["vc"].values())
        self._load_BC(js["bc"].values())
        self._load_drossel(js["drossel"].values())
        self._load_prepost(js["prepost"])

    def _load_check(self, pars):
        checkdes = Checkdes()
        if len(checkdes._pars) != len(pars):
            if len(checkdes._pars_without_optional) != len(pars):
                raise ParsingException(
                    f"Bad number of arguments for checkdes.prm, expected: {len(checkdes._pars)} "
                    f"or {len(checkdes._pars_without_optional)} (without optional), found: {len(pars)}")
            checkdes._pars = checkdes._pars_without_optional

        for par, key in zip(pars, checkdes._pars):
            try:
                checkdes.__setattr__(key, par)
            except ParserException as e:
                raise e

        self.checkdes = checkdes

    def _parse_checkdes(self, path_to_checkdes):

        with open(path_to_checkdes, 'r') as file:
            pars = []
            for line in file.readlines():
                line = line.split('!')[0]
                vals = re.split(r'\s+', line)
                vals = list(filter(lambda x: bool(x), vals))
                if vals:
                    pars.extend(vals)
        self._load_check(pars)

    def _load_AC(self, pars):
        ac = AC()
        if len(ac._pars) != len(pars):
            if len(ac._pars_without_optional) != len(pars):
                raise ParsingException(
                    f"Bad number of arguments for AC.lsc, expected: {len(ac._pars)} "
                    f"or {len(ac._pars_without_optional)} (without optional), found: {len(pars)}")
                ac._pars = ac._pars_without_optional
        for par, key in zip(pars, ac._pars):
            try:
                ac.__setattr__(key, par)
            except ParserException as e:
                raise e

        self.ac = ac

    def _parse_AC(self, path_to_AC):

        with open(path_to_AC, 'r') as file:
            pars = []
            for line in file.readlines():
                line = line.split('!')[0]
                vals = re.split(r'\s+', line)
                vals = list(filter(lambda x: bool(x), vals))
                if vals:
                    pars.extend(vals)
        self._load_AC(pars)

    def _load_VC(self, pars):
        vcs = {}
        for vc_obj in pars:
            vc = VC()
            if len(vc._pars) != len(vc_obj.items()):
                if len(vc._pars_without_optional) != len(vc_obj.items()):
                    raise ParsingException(
                        f"Bad number of arguments for volume named {vc_obj['A1']}, expected: {len(vc._pars)} "
                        f"or {len(vc._pars_without_optional)} (without optional), found: {len(vc_obj.items())}")
                    vc._pars = vc._pars_without_optional

            for key, value in vc_obj.items():
                vc.__setattr__(key, value)
            vcs[vc.A1] = vc
        self.vcs = VCS()
        self.vcs.vcs = vcs

    def _parse_VC(self, path_to_VC):
        vcs = {}

        with open(path_to_VC, 'r') as file:
            lines = ''.join(line for line in file.readlines() if not line.startswith('END'))
        cases = re.split('VC', lines)
        for case in cases:
            if case:
                values = []
                for line in case.splitlines():
                    line = line.split('!')[0]
                    vals = re.split(r'\s+', line)
                    vals = list(filter(lambda x: bool(x), vals))
                    values.extend(vals)

                vc = VC()
                if len(vc._pars) != len(values):
                    if len(vc._pars_without_optional) != len(values):
                        raise ParsingException(
                            f"Bad number of arguments for volume named {values[0]}, expected: {len(vc._pars)} "
                            f"or {len(vc._pars_without_optional)} (without optional), found: {len(values)}")
                        vc._pars = vc._pars_without_optional

                for (key, value) in zip(vc._pars, values):
                    try:
                        vc.__setattr__(key, value)
                    except ParserException as e:
                        raise e

                vcs[vc.A1] = vc

        self.vcs = VCS()

        self.vcs.vcs = vcs

    def _load_BC(self, pars):
        bcs = {}
        for case in pars:

            values = [v for v in case.values()]

            name = values[0]
            bc_type = values[1]
            bc_subtype = values[2]
            bc = BC(name, bc_type, bc_subtype)
            bc["name"] = name
            if bc["_has_pars"]:

                if len(bc["_pars"]) != len(values[3:]):
                    if len(bc["_pars_without_optional"]) != len(values[3:]):
                        raise ParsingException(
                            f"Bad number of arguments for boundry named '{name}', expected: {len(bc['_pars'])} "
                            f"or {len(bc['_pars_without_optional'])} (without optional), found: {len(values[3:])}")
                    bc["_pars"] = bc["_pars_without_optional"]
                pars = bc["_pars"]
                assert len(pars) == len(values[3:])
                for key, val in zip(pars, values[3:]):
                    bc[key] = val
            bcs[name] = bc
        self.bcs = BCS()
        self.bcs.bcs = bcs

    def _parse_BC(self, path_to_BC):
        bcs = {}
        with open(path_to_BC, 'r') as file:
            lines = ''.join(line for line in file.readlines() if not line.startswith('END'))
        cases = re.split('BC', lines)
        for case in cases:
            if case:

                values = []
                for line in case.splitlines():
                    line = line.split('!')[0]
                    vals = re.split(r'\s+', line)
                    vals = list(filter(lambda x: bool(x), vals))
                    values.extend(vals)
                name = values[0]
                bc_type = values[1]
                bc_subtype = values[2]
                bc = BC(name, bc_type, bc_subtype)
                bc["name"] = name
                if bc["_has_pars"]:

                    if len(bc["_pars"]) != len(values[3:]):
                        if len(bc["_pars_without_optional"]) != len(values[3:]):
                            raise ParsingException(
                                f"Bad number of arguments for boundry named '{name}', expected: {len(bc['_pars'])} "
                                f"or {len(bc['_pars_without_optional'])} (without optional), found: {len(values[3:])}")
                        bc["_pars"] = bc["_pars_without_optional"]
                    pars = bc["_pars"]
                    assert len(pars) == len(values[3:])

                    for key, val in zip(pars, values[3:]):
                        bc[key] = val
                bcs[name] = bc
        self.bcs = BCS()
        self.bcs.bcs = bcs

    def _load_drossel(self, pars):
        drossel = Drossel()
        if len(drossel._pars) != len(pars):
            if len(drossel._pars_without_optional) != len(pars):
                raise ParsingException(
                    f"Bad number of arguments for drossel.prm, expected: {len(drossel._pars)} "
                    f"or {len(drossel._pars_without_optional)} (without optional), found: {len(pars)}")

            drossel._pars = drossel._pars_without_optional

        for key, value in zip(drossel._pars, pars):
            drossel.__setattr__(key, value)
        self.drossel = drossel

    def _parse_drossel(self, path_to_drossel):
        with open(path_to_drossel, 'r') as file:
            pars = []
            for line in file.readlines():
                line = line.split('!')[0]
                vals = re.split(r'\s+', line)
                vals = list(filter(lambda x: bool(x), vals))
                if vals:
                    pars.extend(vals)
        self._load_drossel(pars)

    def _load_prepost(self, pars):
        prepost = Prepost()
        if pars["BCuts"] != None or pars["BControls"] != None:
            raise NotImplementedError("Block cuts and Block control has not been implemented yet.")
        xcuts = {}
        for val in pars["XCuts"].values():
            xc = XCut()
            if len(val) != len(xc._pars):
                if len(val) != len(xc._pars_without_optional):
                    raise ParsingException(
                        f"Bad number of arguments for prepost, expected: {len(xc._pars)} "
                        f"or {len(xc._pars_without_optional)} (without optional), found: {len(val)}")
                xc._pars = xc._pars_without_optional

            for key, value in zip(xc._pars, val.values()):
                xc.__setattr__(key, value)
            xcuts[xc.Slice_name] = xc
        prepost.xcuts = xcuts
        self.prepost = prepost


    def _parse_prepost(self, path_to_prepost):
        with open(path_to_prepost, 'r') as file:
            lines = file.readlines()
        first = lines.pop(0)
        b_cuts, b_control, x_cut = first.split()
        if b_cuts != '0' or b_control != '0':
            raise NotImplementedError("Block cuts and Block control has not been implemented yet.")

        try:
            x_cut = int(x_cut)
            if not 0 <= x_cut < float("inf"):
                raise ValueError("")
        except ValueError as e:
            raise ParsingException(f"Bad argument for X_cut in prepost.prm, expected integer > 0, got: {x_cut}")

        lines = list(filter(str.strip, lines))

        if x_cut != len(lines):
            raise ParsingException(f"Bad number of X slices in prepost.prm, expected: {len(lines)}, got: {x_cut}")

        prepost = Prepost()
        xcuts = {}
        for line in lines:
            xc = XCut()
            line = line.split('!')[0].strip()
            pars = re.split(r'\s+', line)

            if len(pars) != len(xc._pars):
                if len(pars) != len(xc._pars_without_optional):
                    raise ParsingException(
                        f"Bad number of arguments for prepost, expected: {len(xc._pars)} "
                        f"or {len(xc._pars_without_optional)} (without optional), found: {len(pars)}")
                xc._pars = xc._pars_without_optional

            for key, value in zip(xc._pars, pars):
                xc.__setattr__(key, value)
            xcuts[xc.Slice_name] = xc

        prepost.xcuts = xcuts



        self.prepost = prepost

    def from_files(self, path_to_dir):
        solver = Solver()
        path_to_checkdes = os.path.join(path_to_dir, 'checkdes.prm')
        path_to_AC = os.path.join(path_to_dir, 'AC.lsc')
        path_to_VC = os.path.join(path_to_dir, 'VC.lsc')
        path_to_BC = os.path.join(path_to_dir, 'BC.lsc')
        path_to_drossel = os.path.join(path_to_dir, 'drossel.prm')
        path_to_prepost = os.path.join(path_to_dir, 'prepost.prm')

        if not os.path.exists(path_to_checkdes):
            raise FileNotFoundException(f"Parser unable to find checkdes.prm in dir: {path_to_dir}")
        if not os.path.exists(path_to_AC):
            raise FileNotFoundException(f"Parser unable to find AC.lsc in dir: {path_to_dir}")
        if not os.path.exists(path_to_VC):
            raise FileNotFoundException(f"Parser unable to find VC.lsc in dir: {path_to_dir}")
        if not os.path.exists(path_to_BC):
            raise FileNotFoundException(f"Parser unable to find BC.lsc in dir: {path_to_dir}")

        self._parse_checkdes(path_to_checkdes)
        self._parse_AC(path_to_AC)
        self._parse_VC(path_to_VC)
        self._parse_BC(path_to_BC)

        if not os.path.exists(path_to_drossel):
            self.drossel = None
        else:
            self._parse_drossel(path_to_drossel)

        if not os.path.exists(path_to_prepost):
            self.prepost = None
        else:
            self._parse_prepost(path_to_prepost)

        return solver

    def to_json(self, path_to_json):
        with open(path_to_json, 'w') as file:
            lines = ''
            d = {}
            d['checkdes'] = self.checkdes.to_dict()
            d['vc'] = self.vcs.to_dict()
            d['ac'] = self.ac.to_dict()
            d['bc'] = self.bcs.to_dict()
            if self.prepost is None:
                d['prepost'] = None
            else:
                d['prepost'] = self.prepost.to_dict()

            if self.drossel is None:
                d['drossel'] = None
            else:
                d['drossel'] = self.drossel.to_dict()

            lines = json.dumps(d)
            file.write(lines)

    def to_files(self, path_to_folder):
        if not os.path.exists(path_to_folder):
            os.mkdir(path_to_folder)
        with open(os.path.join(path_to_folder, 'checkdes.prm'), 'w') as file:
            file.write(self.checkdes.to_checkdes())
        with open(os.path.join(path_to_folder, 'VC.lsc'), 'w') as file:
            file.write(self.vcs.to_checkdes())
        with open(os.path.join(path_to_folder, 'AC.lsc'), 'w') as file:
            file.write(self.ac.to_checkdes())
        with open(os.path.join(path_to_folder, 'BC.lsc'), 'w') as file:
            file.write(self.bcs.to_checkdes())

        if self.prepost is not None:
            with open(os.path.join(path_to_folder, 'prepost.prm'), 'w') as file:
                file.write(self.prepost.to_checkdes())

        if self.drossel is not None:
            with open(os.path.join(path_to_folder, 'drossel.prm'), 'w') as file:
                file.write(self.drossel.to_checkdes())
