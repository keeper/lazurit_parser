import re
from .exceptions import ValueOutOfRangeException, BadTopBottomRangeException, WrongValueFormat


class Descriptor:
    def __get__(self, obj, objtype):
        if obj is None:
            return self
        return obj.__getattribute__(self.attr)


float_pat_1 = r"(-?[\d\.]+)\.\."
float_pat_2 = r"\.\.(-?[\d\.]+)"
float_pat_3 = r"(-?[\d\.]+)\.\.(-?[\d\.]+)"
class Float(Descriptor):
    def __init__(self, name, bot_top = ".."):
        self.val = 0.0
        self.name = name
        self.attr = "_" + name
        if bot_top == "..":
            self.bot = -float("inf")
            self.top = float("inf")
        else:
            match_1 = re.match(float_pat_1, bot_top)
            match_2 = re.match(float_pat_2, bot_top)
            match_3 = re.match(float_pat_3, bot_top)

            try:
                if match_1 and match_1.group(0) == bot_top:
                    x = match_1.group(1)
                    x = float(x)
                    self.bot = x
                    self.top = float("inf")
                elif match_2 and match_2.group(0)  == bot_top:
                    y = match_2.group(1)
                    y = float(y)
                    self.bot = -float("inf")
                    self.top = y
                elif match_3 and match_3.group(0) == bot_top:

                    x = match_3.group(1)
                    y = match_3.group(2)
                    x = float(x)
                    y = float(y)
                    self.bot = x
                    self.top = y
                    if y < x:
                        raise BadTopBottomRangeException(
                            f"Bad range value for var {name}: {bot_top}! Supported formats: .., x.., ..y, x..y")

                else:
                    raise BadTopBottomRangeException(f"Bad range for var {name}: {bot_top}! Supported formats: .., x.., ..y, x..y")
            except ValueError as e:
                raise BadTopBottomRangeException(
                    f"Bad range value for var {name}: {bot_top}! Supported formats: .., x.., ..y, x..y")



    def __set__(self, obj, val):
        if isinstance(val, str):
            if val.startswith("'") and val.endswith("'"):
                val = val[1:-1]
            try:
                new_val = float(val)
            except ValueError as e:
                raise WrongValueFormat(f"Expected value of type Integer for var {self.name}, got: {val}")
            val = new_val
        if  (self.bot <= val <= self.top):
            obj.__dict__[self.attr] = val

        else:
            raise ValueOutOfRangeException(f"Var {self.name} out of range: [{self.bot}; {self.top}].")



int_pat_1 = r"(-?[\d]+)\.\."
int_pat_2 = r"\.\.(-?[\d]+)"
int_pat_3 = r"(-?[\d]+)\.\.(-?[\d]+)"
class Integer(Descriptor):
    def __init__(self, name, bot_top=".."):
        self.val = 0
        self.name = name
        self.attr = "_" + name
        if bot_top == "..":
            self.bot = -float("inf")
            self.top = float("inf")
        else:
            match_1 = re.match(int_pat_1, bot_top)
            match_2 = re.match(int_pat_2, bot_top)
            match_3 = re.match(int_pat_3, bot_top)
            try:
                if match_1 and match_1.group(0) == bot_top:
                    x = match_1.group(1)
                    x = float(x)
                    self.bot = x
                    self.top = float("inf")
                elif match_2 and match_2.group(0) == bot_top:
                    y = match_2.group(1)
                    y = float(y)
                    self.bot = -float("inf")
                    self.top = y
                elif match_3 and match_3.group(0) == bot_top:
                    x = match_3.group(1)
                    y = match_3.group(2)
                    x = float(x)
                    y = float(y)
                    self.bot = x
                    self.top = y
                    if y < x:
                        raise BadTopBottomRangeException(
                            f"Bad range value for var {name}: {bot_top}! Supported formats: .., x.., ..y, x..y")

                else:
                    raise BadTopBottomRangeException(f"Bad range for var {name}: {bot_top}! Supported formats: .., x.., ..y, x..y")
            except ValueError as e:
                raise BadTopBottomRangeException(
                    f"Bad range value for var {name}: {bot_top}! Supported formats: .., x.., ..y, x..y")



    def __set__(self, obj, val):
        if isinstance(val, str):
            if val.startswith("'") and val.endswith("'"):
                val = val[1:-1]
            try:
                new_val = float(val)
            except ValueError as e:
                raise WrongValueFormat(f"Expected value of type Integer for var {self.name}, got: {val}")
            if int(new_val) == new_val:
                val = int(new_val)
            else:
                raise WrongValueFormat(f"Expected value of type Integer for var {self.name}, got: {val}")
        if not isinstance(val, int):
            raise WrongValueFormat(f"Expected value of type Integer for var {self.name}, got: {val}")
        if  (self.bot <= val <= self.top):
            obj.__dict__[self.attr] = val
        else:
            raise ValueOutOfRangeException(f"Var {self.name} out of range: [{self.bot}; {self.top}].")



class String(Descriptor):
    def __init__(self, name):
        self.val = ''
        self.name = name
        self.attr = "_" + name




    def __set__(self, obj, val):
        if not isinstance(val, str):
            raise WrongValueFormat(f"Expected value of type String for var {self.name}, got: {obj}")
        obj.__dict__[self.attr] = val


class Enum(Descriptor):
    def __init__(self, name, possible):

        self.name = name
        self.attr = "_" + name

        if len(possible) == 0 and not isinstance(possible, set) and not isinstance(possible[0], str):
            raise BadTopBottomRangeException(
                f"Expected some set of strings possible values for var {name}, got: {possible} ")

        self.possible = possible





    def __set__(self, obj, val):
        if not isinstance(val, str):
            raise WrongValueFormat(f"Expected value of type String, got: {val}")
        is_in = val in self.possible
        # for v in self.possible:
        #     print(v, val)
        #     if v == val:
        #         is_in = True
        if is_in:
            obj.__dict__[self.attr] = val
        else:
            raise ValueOutOfRangeException(f"Var {self.name}, value {val} is not in set {self.possible}.")

class Optional(Float):
    def __init__(self, name, bot_top = ".."):
        super().__init__(name, bot_top)






































