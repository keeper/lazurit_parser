import click
from .parser import Parser
import os
from .exceptions import ParserException, FileNotFoundException


@click.group()
def cli():
    pass


@cli.command('to_json')
@click.argument('path', type=click.Path(exists=True), metavar="Path_to_dir")
def to_json(path):
    """Parses directory's chechdes,... files and creates lazurit.json file in it."""
    p = Parser()
    try:
        p.from_files(path)
        p.to_json(os.path.join(path, "lazurit.json"))
    except ParserException as e:
        print(e.message)
        return


@cli.command('from_json')
@click.argument('path', type=click.Path(exists=True), metavar="Path_to_json")
def from_json(path):
    """Parses json file and creates """
    p = Parser()
    try:
        p.from_json(path)
        path = os.path.dirname(path)
        p.to_files(path)
    except ParserException as e:
        print(e.message)
        return


@cli.group()
def set():
    """Command for seting parameters in current directory, use --help for more info."""
    pass


@set.command('CD')
@click.argument('par_name', required=True, metavar="Parameter_Name", type=str)
@click.argument('value', required=True, metavar="New_Value")
def set_checkdes(par_name, value):
    """Command for seting CHECKDES parameters in current directory, use --help for more info."""
    cwd = os.getcwd()
    try:
        p = try_open(cwd)
    except ParserException as e:
        print(e.message)
        return

    try:
        p.checkdes.__setattr__(par_name, value)
        p.to_files(cwd)
        p.to_json(os.path.join(cwd, "lazurit.json"))
    except ParserException as e:
        print(e.message)
        return


def try_open(directory) -> Parser:
    p = Parser()
    try:
        p.from_files(directory)
    except FileNotFoundException as e:
        p.from_json(os.path.join(directory, "lazurit.json"))

    return p


@set.command('BC')
@click.argument('bc_name', required=True, metavar="Boundry_Name", type=str)
@click.argument('par_name', required=True, metavar="Parameter_Name", type=str)
@click.argument('value', required=True, metavar="New_Value")
def set_bc(bc_name, par_name, value):
    """Command for setting BC parameters in current directory, use --help for more info."""
    cwd = os.getcwd()
    try:
        p = try_open(cwd)
    except ParserException as e:
        print(e.message)
        return

    try:
        p.bcs.bcs[bc_name][par_name] = value
        p.to_files(cwd)
        p.to_json(os.path.join(cwd, "lazurit.json"))
    except ParserException as e:
        print(e.message)
        return


@set.command('VC')
@click.argument('vc_name', required=True, metavar="Volume_Name", type=str)
@click.argument('par_name', required=True, metavar="Parameter_Name", type=str)
@click.argument('value', required=True, metavar="New_Value")
def set_vc(vc_name, par_name, value):
    """Command for setting VC parameters in current directory, use --help for more info."""
    cwd = os.getcwd()
    try:
        p = try_open(cwd)
    except ParserException as e:
        print(e.message)
        return

    try:
        p.vcs.vcs[vc_name].__setattr__(par_name, value)
        p.to_files(cwd)
        p.to_json(os.path.join(cwd, "lazurit.json"))
    except ParserException as e:
        print(e.message)
        return


@set.command('AC')
@click.argument('par_name', required=True, metavar="Parameter_Name", type=str)
@click.argument('value', required=True, metavar="New_Value")
def set_vc(par_name, value):
    """Command for setting AC parameters in current directory, use --help for more info."""
    cwd = os.getcwd()
    try:
        p = try_open(cwd)
    except ParserException as e:
        print(e.message)
        return

    try:
        # p.vcs.vcs[bc_name][par_name] = value
        p.ac.__setattr__(par_name, value)
        p.to_files(cwd)
        p.to_json(os.path.join(cwd, "lazurit.json"))
    except ParserException as e:
        print(e.message)
        return
