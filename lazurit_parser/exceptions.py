
class ParserException(Exception):
    def __init__(self, message):
        self.message = message
        super().__init__(message)

class FileNotFoundException(ParserException):
    def __init__(self, message):
        self.message = message
        super().__init__(message)


class ValueOutOfRangeException(ParserException):
    def __init__(self, message):
        self.message = message
        super().__init__(message)


class BadTopBottomRangeException(ParserException):
    def __init__(self, message):
        self.message = message
        super().__init__(message)


class WrongValueFormat(ParserException):
    def __init__(self, message):
        self.message = message
        super().__init__(message)

class ParsingException(ParserException):
    def __init__(self, message):
        self.message = message
        super().__init__(message)