import json, os
# from .parser import Parser
from .descriptors import Float, Integer, String, Enum, Descriptor, Optional
from .exceptions import BadTopBottomRangeException, ValueOutOfRangeException


class Meta(type):
    def __new__(cls, name, bases, dct):
        path = os.path.join(os.path.dirname(__file__), 'lazurit.json')
        with open(path, 'r') as f:
            lines = ''.join(f.readlines())
        js = json.loads(lines)
        for par in js[name]:
            if par['type'] == "Float":
                dct[par["name"]] = Float(par["name"], par["valid_value"])
                dct["_" + par["name"]] = 0.0

            if par['type'] == "Integer":
                dct[par["name"]] = Integer(par["name"], par["valid_value"])
                dct["_" + par["name"]] = 0

            if par['type'] == "String":
                dct[par["name"]] = String(par["name"])
                dct["_" + par["name"]] = ''

            if par['type'] == "Enum":
                dct[par["name"]] = Enum(par["name"], set(par["valid_value"]))
                dct["_" + par["name"]] = ''
            if par['type'] == "Optional":
                dct[par["name"]] = Optional(par["name"], par["valid_value"])
                dct["_" + par["name"]] = 0.0

        dct['_pars'] = list(par["name"] for par in js[name])
        dct['_pars_without_optional'] = list(par["name"] for par in js[name] if par["type"] != "Optional")

        return super(Meta, cls).__new__(cls, name, bases, dct)

class MetaBoundry(type):
    def __new__(cls, name, bases, dct):
        path = os.path.join(os.path.dirname(__file__), 'boundry.json')
        with open(path, 'r') as f:
            lines = ''.join(f.readlines())
        js = json.loads(lines)
        _types = dct
        for key_type, val in js.items():
            _types[key_type] = {}
            for key_subtype, values in val.items():
                _types[key_type][key_subtype] = {}

                if len(values) == 0:
                    _types[key_type][key_subtype]["_has_pars"] = False
                else:
                    _types[key_type][key_subtype]["_has_pars"] = True
                for value in values:
                    if value['type'] == "Float":
                        _types[key_type][key_subtype][value["name"]] = Float(value["name"], value["valid_value"])
                        dct["_" + value["name"]] = 0.0
                    if value['type'] == "Optional":
                        _types[key_type][key_subtype][value["name"]] = Optional(value["name"], value["valid_value"])
                        dct["_" + value["name"]] = 0.0

                    if value['type'] == "Integer":
                        _types[key_type][key_subtype][value["name"]] = Integer(value["name"], value["valid_value"])
                        dct["_" + value["name"]] = 0.0

                    if value['type'] == "String":
                        _types[key_type][key_subtype][value["name"]] = String(value["name"])
                        dct["_" + value["name"]] = 0.0

                    if value['type'] == "Enum":
                        _types[key_type][key_subtype][value["name"]] = Enum(value["name"], set(value["valid_value"]))
                        dct["_" + value["name"]] = 0.0

                _types[key_type][key_subtype]["_pars"] = [value["name"] for value in values]
                _types[key_type][key_subtype]["_pars_without_optional"] = [value["name"] for value in values if value['type'] != "Optional"]
        dct["_types"] = _types
        return super(MetaBoundry, cls).__new__(cls, name, bases, dct)


class BCS:
    def to_json(self):
        res = {}
        for key, value in self.bcs.items():
            res[key] = value.to_dict()
        return json.dumps(res)

    def to_dict(self):
        res = {}
        for key, value in self.bcs.items():
            res[key] = value.to_dict()
        return res
    def to_checkdes(self):
        res = ''
        for key, value in self.bcs.items():
            res += 'BC\n'
            for key, value in value.to_dict().items():
                res += f" {value} ! {key}\n"
        return res


class BC(metaclass=MetaBoundry):
    def __init__(self, name, type, subtype):
        self.name = name
        self.type = type
        self.subtype = subtype
    def to_dict(self):
        d = {"name": self.name, "type": self.type, "subtype": self.subtype}
        for key in self._types[self.type][self.subtype]["_pars"]:
            d[key] = self.__getattr__(key)
        return d
    def __getitem__(self, item):
        return self.__getattr__(item)

    def __getattr__(self, item):
        if not item in ["_types", "name", "type", "subtype"]:
            item = self._types[self.type][self.subtype][item]
            if issubclass(item.__class__, Descriptor):
                return item.__get__(self, BC)
            return item
        else:
            return super().__getattribute__(item)

    def __setattr__(self, key, value):

        if  key in ["_types", "name", "type", "subtype"]:
            super().__setattr__(key, value)
        else:
            item = self._types[self.type][self.subtype][key]
            if issubclass(item.__class__, Descriptor):
                item.__set__(self, value)
            else:
                self._types[self.type][self.subtype][key] = value


    def __setitem__(self, key, value):
        self.__setattr__(key, value)
        #if not key in ["name", "type", "subtype"]:
        #    self._types[self.type][self.subtype][key].__set__(self, value)
        #else:
        #    self.__setattribute__(key, value)



class Checkdes(metaclass=Meta):
    def to_json(self):
        return json.dumps(dict((key, self.__getattribute__(key)) for key in self._pars))
    def to_dict(self):
        return dict((key, self.__getattribute__(key)) for key in self._pars)
    def to_checkdes(self):
        res = ''
        for key in self._pars:
            res += f'{str(self.__getattribute__(key))} ! {key}\n'
        return res


class AC(metaclass=Meta):
    def to_json(self):
        return json.dumps(dict((key, self.__getattribute__(key)) for key in self._pars))
    def to_dict(self):
        return dict((key, self.__getattribute__(key)) for key in self._pars)
    def to_checkdes(self):
        res = ''
        for key in self._pars:
            res += f"{self.__getattribute__(key)} ! {key}\n"
        return res


class VC(metaclass=Meta):
    def __init__(self):
        pass
    def to_dict(self):
        return dict((key, self.__getattribute__(key)) for key in self._pars)
    def to_files(self):
        res= ''
        for key, value in dict((key, self.__getattribute__(key)) for key in self._pars).items():
            res += f' {value} ! {key}\n'
        return res

    def __getitem__(self, item):
        return self.__getattr__(item)

class VCS:

    def to_json(self):
        res = []
        for vc in self.vcs:
            res += [vc.to_dict()]
        return json.dumps(res)
    def to_dict(self):
        res = {}
        for vc in self.vcs.values():
            res[vc.A1] = vc.to_dict()

        return res
    def to_checkdes(self):
        res = ''
        for vc in self.vcs.values():
            res += 'VC\n'

            res += vc.to_files()
        res += 'END'
        return res

class XCut(metaclass=Meta):
    def to_json(self):
        return json.dumps(dict((key, self.__getattribute__(key)) for key in self._pars))
    def to_dict(self):
        return dict((key, self.__getattribute__(key)) for key in self._pars)
    def to_checkdes(self):
        res = ''
        for key in self._pars:
            res += f'{str(self.__getattribute__(key))} '
        res += '!'
        for key in self._pars:
            res += f'{key} '
        return res


class Drossel(metaclass=Meta):
    def to_json(self):
        return json.dumps(dict((key, self.__getattribute__(key)) for key in self._pars))
    def to_dict(self):
        return dict((key, self.__getattribute__(key)) for key in self._pars)
    def to_checkdes(self):
        res = ''
        for key in self._pars:
            res += f'{str(self.__getattribute__(key))} ! {key}\n'
        return res

class Prepost:
    def to_json(self):
        return json.dumps(self.to_dict())

    def to_dict(self):
        res = {
            'BCuts': None,
            'BControls': None,
            'XCuts': {}
        }

        for xcut in self.xcuts.values():
            res['XCuts'][xcut.Slice_name] = xcut.to_dict()

        return res
    def to_checkdes(self):
        res = f'0 0 {len(self.xcuts)}\n'
        for xcut in self.xcuts.values():
            res += xcut.to_checkdes() + '\n'

        return res