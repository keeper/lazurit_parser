import click
from click.testing import CliRunner
from ..cli import cli, to_json
import os

def test_t0_json():
    path = os.path.dirname(__file__)
    case = os.path.join(path, "case/test_to_json")
    runner = CliRunner()
    result = runner.invoke(cli, ["to_json", case])

    print(result.output)
    assert result.exit_code == 0

def test_to_json():
    path = os.path.dirname(__file__)
    case = os.path.join(path, "case/test_from_json/lazurit.json")
    runner = CliRunner()
    result = runner.invoke(cli, ["from_json", case])

    print(result.output)
    assert result.exit_code == 0




def test_set_bc():
    runner = CliRunner()
    result = runner.invoke(cli, ["set", "BC", "test1", "Ptot", "0.5"])

    print(result.output)
    assert result.exit_code == 0