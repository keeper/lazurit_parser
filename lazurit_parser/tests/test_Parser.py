import pytest
import json
import pandas as pd

import os
from ..parser import Parser
from ..descriptors import Float, Integer
from ..exceptions import BadTopBottomRangeException, ValueOutOfRangeException
from ..lazurit import Checkdes, BC, XCut, Drossel


class TestMeta:
    def test_1(self):

        t = Checkdes()
        print(t.upwind1)
        print(t.const_2)
        t.upwind1 = 1.5
        print(t.upwind1)
    def test(self):
        l = "A0 A1 A2 vdata_1 vdata_5 vdata_2 vdata_3 vdata_4 vdata_6 TP1 TP2"
        res = []
        for s in l.split(" "):
            res += [dict(name = s,type = "Float", valid_value = "..")]

        print(json.dumps(res))

    def test_prepost(self):
        c = XCut()
        pars = "Zout1 Z .False. 35. -40. -377.5 200.".split()
        for key, value in zip(c._pars, pars):
            c.__setattr__(key, value)

        print()
        print(c.to_checkdes())
        print(c.to_json())
        print(c.to_dict())

    def test_drossel(self):
        d = Drossel()
        pars = "'YZ' 50 50 200 25 175 0.6666667".split()
        for key, value in zip(d._pars, pars):
            d.__setattr__(key, value)
        print()
        print(d.to_checkdes())
        print(d.to_json())
        print(d.to_dict())

class TestParser:
    def test_base(self):
        p = Parser()
        path = os.path.dirname(__file__)
        case = os.path.join(path, "case")
        p.from_files(case)
        p.to_json(os.path.join(case, "test.json"))
        p.to_files(os.path.join(case, "test_to_json"))

    def test_json(self):
        p = Parser()
        path = os.path.dirname(__file__)
        test = os.path.join(path, "case/test.json")
        out = os.path.join(path, "case/test_from_json")
        p.from_json(test)
        p.to_files(out)

    def test_some(self):
        p = Parser()
        path = os.path.dirname(__file__)
        test = os.path.join(path, "case/test_change_some")
        p.from_files(test)
        p.ac.GAM = 1.5
        p.checkdes.Problem_name = "'VZ_2'"
        p.checkdes.UGrid = "'ON'"
        p.to_files(os.path.join(test, 'out'))





class TestBoundry:
    def test_1(self):
        bc = BC("BC1", "Total_Pressure", "Cartesian")
        bc.P0 = 1.0
        print(bc.P0)



class TestDescriptors:
    def test_throw_wrong_range(self):
        try:
            class Test:
                f = Float("HUI", '1.a0..')
        except BadTopBottomRangeException as e:
            return
        assert False

    def test_float_out_of_range(self):
        class Test:
            f = Float("HUI", '1.0..2.0')

        p = Test()
        try:
            p.f = 0.5
        except ValueOutOfRangeException as e:
            return
        assert False

    def test_float(self):
        class Test:
            f = Float("HUI", '1.0..')

        p = Test()
        p.__setattr__('f', 1.5)
        assert p.f == 1.5



class TestTest():
    def test_1(self):
        print("")
        class Desr:
            def __get__(self, instance, owner):
                print(instance, owner)
                return self
            def __set__(self, instance, value):
                print(instance)
                return None

        class Te:
            t = Desr()
            def __getitem__(self, item):
                return self.__getattribute__(item)


        t = Te()
        print(t["t"])



