from setuptools import setup, find_packages

setup(
    name="lazurit_parser",
    version="0.1.3",
    author="Nanopro",
    author_email="nanopro1g@gmail.com",
    description="Lazurit files parser",
    long_description='Lazurit files parser',
    long_description_content_type="text/markdown",
    url="https://gitlab.com/Nanopro/lazurit_parser",
    packages=find_packages(),
    include_package_data=True,
    install_requires = [
        'Click',

    ],
    entry_points={
        'console_scripts': [
            'lazurit = lazurit_parser.cli:cli',
        ]
    },

)
